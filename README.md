# Global model

## About the Project

The project aims to create a global model, which simulates Laser Shock Peening. Sensitivity analysis is conducted. Time-evolution of the main parameters is visualized.

## Built With

The project is created with Python using Spyder IDE.