# -*- coding: utf-8 -*-
#import matplotlib.pylab as plt
from numpy import zeros,exp,argmax,array,trapz,column_stack,append
from scipy.optimize import fsolve
#import sys

t=100.0; dt=0.05; n=int(t/dt+1)


# Unshocked parameters for water and metal
rm0=2700.0; Dm0=5328.0; Sm=1.338; Qc=11.3e6
rw0=1000.0; Dw0=2393.0; Sw=1.333; Qw=2.257e6
P0=1.0e5; rplasma=340.0; Imax=1.0; E=3.0
gamma=1.3;  AP=0.5;  Pplasma=10.0
#print a,'parameters:\n','rm0 =',rm0,'kg/m^3\nDm0 =',Dm0,'m/s\nSm =',Sm,'\nCoating material - Al:','\nrc =',rc,'kg/m^3\nQc =',Qc,'MJ/kg'



    # System of equations
def f(x):
    global Ep,Epk,MFw,MFm,wp,emf,UPL,UPR,inUPL1,inUPR1
    
    Dw=x[0]; Uw=x[1]; rw=x[2]; Pw=x[3]; Ew=x[4]
    Dm=x[5]; Um=x[6]; rm=x[7]; Pm=x[8]; Em=x[9]
    Pp=x[10]; rp=x[11]; UPL[i]=x[12]; UPR[i]=x[13]
    
    f=zeros(14)

    Ep[i] = (gamma*Pp)/((gamma-1.0)*rp)
    Epk[i] = ((UPL[i])**2.0 + (UPR[i])**2.0 - UPL[i]*UPR[i])/6.0
    MFw[i] = rw*(UPL[i]-Uw)
    MFm[i] = rm*(UPR[i]-Um)
    wp[i] = Pp*(UPL[i]+UPR[i])
    emf[i] = MFm[i]*(Epk[i]/P0 + Ep[i] - (((Um)**2.0)/2.0)/P0 - Qc/P0)\
    + MFw[i]*(Epk[i]/P0 + Ep[i] - (((Uw)**2.0)/2.0)/P0 - Qw/P0)

    inUPL1=trapz(UPL,x=l)
    inUPR1=trapz(UPR,x=l)
    
    f[0] = rw0/rw - 1.0 + Uw/Dw
    f[1] = Pw - 1.0 - rw0*Dw*Uw/P0
    f[2] = 2.0*Ew/P0 + (Uw**2.0)/P0 - (Pw + 1.0)*(1.0/rw0 - 1.0/rw)
    f[3] = Dw - Dw0 - Sw*Uw
    f[4] = rm0/rm - 1.0 + Um/Dm
    f[5] = Pm - 1.0 - rm0*Dm*Um/P0
    f[6] = 2.0*Em/P0 + (Um**2.0)/P0 - (Pm + 1.0)*(1.0/rm0 - 1.0/rm)
    f[7] = Dm - Dm0 - Sm*Um
    f[8] = MFw[i] - rp*UPL[i]
    f[9] = MFm[i] - rp*UPR[i]
    f[10] = Pp + rp*UPR[i]*Um/P0 - Pm
    f[11] = Pp + rp*UPL[i]*Uw/P0 - Pw
    f[12] = rp*(inUPL+inUPL1 + inUPR+inUPR1)*(1.0e-9)*dt - (trapz(MFw,x=o) + trapz(MFm,x=o))*(1.0e-9)*dt
    f[13] = (Epk[i] + Ep[i]*P0)*rp*(inUPL+inUPL1 + inUPR+inUPR1)*(1.0e-9)*dt + P0*trapz(wp,x=o)*(1.0e-9)*dt - P0*trapz(emf,x=o)*(1.0e-9)*dt - AP*inI*(1.0e4)*dt
    return f


    # Laser intensity profile
def I(m,duration=int(100/dt+1)):
    global Imax,dt
    I=zeros(m)
    if m<duration:    
        h=m
    else:
        h=duration
    for i in range (h):
        #experiment
        I[i]=Imax*exp(-(exp(-(i*dt-20.0)/8.9))-(i*dt-20.0)/8.9+1.0) 
        #I[i]=Imax*exp(-(exp(-(i*dt-12.5)/5.0))-(i*dt-12.5)/5.0+1.0)           
    return I


o=zeros(n)
for i in range (n):
    o[i]=i
Imax=E/(0.01*trapz(I(n),x=o)*dt)


for u in range (200,500,1):
    print (u)
    rplasma=float(u)
    
    # Arrays inicialization  
    MFw=zeros(2); MFm=zeros(2); UPL=zeros(2); UPR=zeros(2); lm=zeros(2)
    emf=zeros(2); Ep=zeros(2); Epk=zeros (2); lp=zeros(2); wp=zeros(2)
    
    inI=0.0; inUPL1=0.0; inUPL=0.0; inUPR1=0.0; inUPR=0.0; flag=0
    
    zero14=array([0,0,0,0,0,0,0,0,0,0,0,0,0,0])
    
    # Array with results, parameters at t=0
    x=zeros ([14,2])
    x[0,0]=Dw0    #Dw
    x[1,0]=0     #Uw
    x[2,0]=rw0     #rw
    x[3,0]=1.0     #Pw
    x[4,0]=0.0      #Ew
    x[5,0]=Dm0    #Dm
    x[6,0]=0     #Um
    x[7,0]=rm0    #rm
    x[8,0]=1.0     #Pm
    x[9,0]=0.0       #Em
    x[10,0]=Pplasma    #Pp 
    x[11,0]=rplasma    #rp
    x[12,0]=0      #UpL
    x[13,0]=0       #UpR
    
    
    # Initial guess for unknowns
    g=x[:,0].copy()
    
    
    UPR[1]=g[13]; UPL[1]=g[12];  #Um[1]=g[6]; Uw[1]=g[1]
    
    Ep[1]=(gamma*Pplasma)/((gamma-1.0)*rplasma)
    Epk[1]=(((UPL[1])**2.0) + ((UPR[1])**2.0) - UPL[1]*UPR[1])/6.0
    wp[1]=g[10]*(UPL[1]+UPR[1])
    MFw[1]=rw0*(UPL[1]-g[1])
    MFm[1]=rm0*(UPR[1]-g[6])
    emf[1]=MFm[1]*(Epk[1]/P0 + Ep[1] - (((g[6])**2)/2.0)/P0 - Qc/P0) + MFw[1]*(Epk[1]/P0 + Ep[1] - (((g[1])**2)/2.0)/P0 - Qw/P0)
    
    # Solving system at different time moments
    for i in range (1,n):
        
        o=zeros(i+1); l=zeros(i+1); l[i]=1
        for j in range (i+1):
            o[j]=j 
        inI= inI+trapz(I(i+1),x=l)        
    
        g[11]=rplasma
    
        g,inf,ier,msg=fsolve (f,g,full_output=True,xtol=1.0e-13,maxfev=0)
    
    
        if ier!=1:
            flag=flag+1
            print ('Step #',i,' of ', int(t/dt), ' error ','%6.1f' %(flag*100/int(t/(10*dt))), ' % ')
        #print (g[11], inI)
        if flag==int(t/(10*dt)): 
            print (' ERROR ')
            break        
        
        inUPL=inUPL+inUPL1
        inUPR=inUPR+inUPR1
    
        UPR[i]=g[13]; UPL[i]=g[12]
       # Um[i]=g[6]; Uw[i]=g[1]
        
        wp[i]=g[10]*(UPL[i] + UPR[i])
        MFw[i]=g[2]*(UPL[i] - g[1])
        MFm[i]=g[7]*(UPR[i] - g[6])
        Ep[i]=(gamma*g[10])/((gamma - 1.0)*g[11])
        Epk[i]=(((UPL[i])**2.0) + ((UPR[i])**2.0) - UPL[i]*UPR[i])/6.0
        emf[i]=MFm[i]*(Epk[i]/P0 + Ep[i] - (((g[6])**2.0)/2.0)/P0 - Qc/P0)\
        + MFw[i]*(Epk[i]/P0 + Ep[i] - (((g[1])**2.0)/2.0)/P0 - Qw/P0)
    
    
        # Saving the results 
        x[:,i]=g.copy()
    
        #lm[i]=trapz(UPR,x=o)*dt - trapz(Um,x=o)*dt
       # lp[i]=trapz(UPR,x=o)*dt + trapz(UPL,x=o)*dt
    
        if i!=(n-1):
            x=column_stack((x,zero14))
            UPR=append(UPR,[0])
            UPL=append(UPL,[0])
            wp=append(wp,[0])
            MFw=append(MFw,[0])
            MFm=append(MFm,[0])
            Ep=append(Ep,[0])
            Epk=append(Epk,[0])
            emf=append(emf,[0])
           # lm=append(lm,[0])
           # lp=append(lp,[0])
            
    x[2,:]=x[2,:]; x[3,:]=x[3,:]*P0; x[7,:]=x[7,:]
    x[8,:]=x[8,:]*P0; x[10,:]=x[10,:]*P0; Ep[:]=Ep[:]*P0
    MFw[:]=x[2,:]*(UPL[:] - x[1,:]); MFm[:]=x[7,:]*(UPR[:] - x[6,:])     
        
    #Output max parameters
    #print '\n',dt,'\nMaximum parameters:'
    k=argmax(x[10,:])
    #for i in range (14):
    #    print a[i]+' = '+'%6.1f' %x[i,k]+' '+c[i]
    #print 'Ep = '+'%6.1f' %Ep[k]+' J/kg'
    
    #k1=argmax(I(n))*dt
    #print 'Max laser intensity at',k1,'ns'
    #print 'Max pressure at ',k*dt,' ns'
    
    o=zeros(n)
    for i in range (n):
        o[i]=i
        
    #energy=trapz(I(n),x=o)*(1.0e-2)*dt
    #print '%5.4f' %energy
    
    fileob=open('out/out_inter_E3_300ns_1x1.txt','a')
    if x[10,k]/1.0e9>0.1 and x[4,k]>0 and x[10,k]/1.0e9<15.0:
        fileob.write(str(u)+' '+str('{:4.2f}'.format(x[10,k]/1.0e9))+' '+str('{:4.2f}'.format(x[11,k]))+'\n')
    fileob.close()


'''
# Plotting time-dependent parameters
st='Parameters '+b
plt.figure(123, figsize=(15.0,13.0))
plt.subplots_adjust (left=0.08, bottom=0.08, right=0.98, top=0.97, wspace=0.2, hspace=0.35)

plt.subplot (3,2,1)
st='Imax='+str(Imax)+' $GW/cm^2$'
plt.plot(o*dt,I(n)/I(n)[argmax(I(n))],'r-',label=st,linewidth=1.5)
plt.grid()
plt.legend()
plt.title('Laser intensity',fontsize=18)
plt.xlabel('Time, $ns$', fontsize=16)
plt.ylabel('Intensity',fontsize=16)


#for i in range (0,n):
#    x[8,i]=x[8,i]/x[8,k]


for i in range (int(60/dt+1),n):
    x[8,i]=x[8,i]*((i*dt/60.0)**(-1.5))

plt.subplot (3,2,2)
plt.plot(o*dt,x[8,:],'r-',linewidth=1.5)
plt.grid()
plt.legend()
plt.title('Pressure in metal',fontsize=18)
plt.xlabel('Time, $ns$',fontsize=16)
plt.ylabel('Pressure, $GPa$',fontsize=16)

plt.subplot (3,2,3)
plt.plot(o*dt,x[1,:n],'r-',label='Uw',linewidth=1.5)
plt.plot(o*dt,x[12,:n],'b-',label='UpL',linewidth=1.5)
plt.grid()
plt.legend()
plt.title('Velocity of water',fontsize=18)
plt.xlabel('Time, $ns$',fontsize=16)
plt.ylabel('Speed, $m/s$',fontsize=16)

plt.subplot (3,2,4)
plt.plot(o*dt,x[6,:n],'r-',label='Um',linewidth=1.5)
plt.plot(o*dt,x[13,:n],'b-',label='UpR',linewidth=1.5)
plt.grid()
plt.legend()
plt.title('Velocity of metal',fontsize=18)
plt.xlabel('Time, $ns$',fontsize=16)
plt.ylabel('Speed, $m/s$',fontsize=16)

plt.subplot (3,2,5)
plt.plot(o*dt,MFw[:n]/1.0e4,'r-',linewidth=1.5)
plt.grid()
plt.legend()
plt.title('Water-plasma mass flow',fontsize=18)
plt.xlabel('Time, $ns$',fontsize=16)
plt.ylabel('Massflow, $kg/s*sm^2$',fontsize=16)

plt.subplot (3,2,6)
plt.plot(o*dt,MFm[:n]/1.0e4,'r-',linewidth=1.5)
plt.grid()
plt.legend()
plt.title('Metal-plasma mass flow',fontsize=18)
plt.xlabel('Time, $ns$',fontsize=16)
plt.ylabel('Massflow, $kg/s*sm^2$',fontsize=16)

plt.subplot (4,2,7)
plt.plot(o*dt,lp/1.0e6,'b-',linewidth=1.5)
plt.grid()
plt.legend()
plt.title('Plasma layer thickness')
plt.xlabel('Time, $ns$',fontsize=16)
plt.ylabel('Thickness, $mm$',fontsize=16)

plt.subplot (4,2,8)
plt.plot(o*dt,lm/1.0e3,'b-',linewidth=1.5)
plt.grid()
plt.legend()
plt.title('Vaporised coating layer thickness')
plt.xlabel('Time, $ns$',fontsize=16)
plt.ylabel('Thickness, $micron$',fontsize=16)

st='Current_result_'+b+'.png'
plt.savefig(st)
plt.show ()'''

